const locators = {
appUrl: 'https://www.latrobedirect.com/latrobe',
registrationButton: '//*[@id="frmRoot_btnRegistrationButton"]',
openNewAccountButton: '//*[@id="fAccount_Opt0Button"]',
productDiscloserLink: '//*[@id="fAccount_btnPDSButton"]',
productDiscloserYes: '//*[@id="fAccount_Opt12Button"]',
addPersonalDetailsButton: '//*[@id="fAccount_btnNextButton"]',
givenNameInput: '//*[@id="fRegistration_txtGivenNameEdit"]',
middleNameInput: '//*[@id="fRegistration_txtMiddleName"]',
familyNameInput: '//*[@id="fRegistration_txtFamilyNameEdit"]',
dobDay: '//*[@id="fRegistration_txtDayEdit"]',
dobMonth: '//*[@id="fRegistration_txtMonthEdit"]',
dobYear: '//*[@id="fRegistration_txtYearEdit"]',
addressInput: '//*[@id="fRegistration_txtResAddressEdit"]'
}
module.exports = locators